Create Users
============

```yaml
auth_keys:
  - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8/7yn31pgkoGKyKcr1OmZZ57/uJwnVhOk/quk99OZSLohroYsCI7uPDVeWAls2gUwEQHZTXCMsby1TU0kDI+gaZfJ7fIYtDs/2pbsxeH2Qh4Ddo1S6Tcw6d9wFL7cukdpVdbWko2jMih6AVLdAYuj12Op+ucbfGv0zK/bjfkcr1dBUVisw7YMPRUKfyM162Ty9cM0G8rDpw48fNpc4uvdbgciE5PATwOoyODQ3TIHHt+A/MT1ffvMNgMvKemR7NLJVr+s3WGw0Cnp8le47XcGsdWEENXWJ5PqF6DDm6Lf4TZyZ0BG9vawHPSvhBdz6gmAbsNgIa1q7wcGmsD5tuLv AlekseiSmirnov"
```

Example Playbook
----------------

    - hosts: all
      roles:
         - create_user
