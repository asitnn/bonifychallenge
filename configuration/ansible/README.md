How to run
==========

Update the user list in the group_vars/all/users.yaml

Run one of the following commands

to remote instances
```bash
ansible-playbook -c local -i localhost, playbook.yml
```

to local instance
```bash
ansible-playbook -i inventory playbook.yml -t create_user
```

#### Example. Inventory file
```bash
[aws]
10.17.1.188
10.17.1.189
10.17.1.190
```

NOTE:
* To provide SUDO password add -K to ansible-playbook command, if it necessary.
* To use the 'ssh' connection type with passwords, you must install the sshpass program
* To remove host from known hosts file run following: ssh-keygen -f "/home/<local_username>/.ssh/known_hosts" -R <server_address>
