## Answers:

### A1:
- messages in a queue memory and/or CPU
- memory and/or CPU
- memory and/or CPU
- memory and/or CPU

### A2:
- User Passwords(actual clients): 
Use a hash function. Store "salted and hashed" password in a database

- SysAdmin passwords
Any password manager like KeePass.

- Passwords for connections to DBs, which are used by backend services
HashiCorp Vault

- Docker file configurations that should include passwords
Docker secrets. HashiCorp Vault

- SSL/Encryption Keys
HashiCorp Vault


### A3:
skipped

### A4:
Send logs to a centralized storage like Elasticsearch Service + Logstash and Kibana.
Include a unique id in the response and correlate requests with a that unique id.

### A5:
![alt text](img/ci-cd-pipeline.png "CI/CD Pipeline")

### A6:
[HOW TO RUN](configuration/ansible/README.md)