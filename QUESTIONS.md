# Senior DevOps Engineer
## Challenge Introduction

### Please answer questions with the following instructions:
* Questions 1-2: Must be answered all
* Questions 3-4-5: Choose two out of three questions
* Questions 6: Must be answered all

**If you decide to answer all the questions it is always good.**

**Please try to give a really detailed answer as much as you can including the names of tools, approaches, and assumptions.**

**Thank you for your time and Good Luck!**

### Story(Concise)
* AWS VPC
* Complex microservices architecture with 30+ backend services
* RDBMS Database
* NoSQL Database
* Backend - Java
* Frontend - React.js
* Docker + Kubernetes
* Data lake

### Question 1(Level 1) (15 min):
#### What triggers for scalability would you use for:
* Backend servers
* Database performance
* Message Bus performance(RabbitMQ)
* Docker + Kubernetes

### Question 2(Level 1) (15 min):
In case you run on the environment without password management capabilities(you rented actual hardware servers) and you need to build such on your own.
What toolset or custom solution would you use for managing credentials and keys?
Please relate to:
* User Passwords(actual clients)
* SysAdmin passwords
* Passwords for connections to DBs, which are used by backend services
* Docker file configurations that should include passwords
* SSL/Encryption Keys

Any creative solutions are welcome, please describe where exactly it could be saved for each point.

### Question 3(Level 2) (1 hour):
Please define the best approach for real-time flow data which needs to sync user(FE events for example) to external CRM and in parallel to put data in the data lake. What toolset or custom solution would you use for managing the data lake? Please relate to:
* Data Storage
* Data Manipulation
* Data Security and control layer
* Data Transformation

### Question 4(Level 2) (1 hour):
Please describe how you aggregate logs in Microservices architecture system from:
* Frontend
* BackEnd
* DBs
* Message Bus Please describe what types of logs would you store and what tools you would use for accessing them
* You can use a diagram or textual description

### Question 5(Level 2) (1 hour):
Draw the most simple build & deploy process including CI\CT\CQ\CD for Java application from the developer’s computer to the production server in AWS(which runs in Kubernetes cluster)

### Question 6(Level 3)(1 hour):
Please write automation for the process of granting/revoking SSH access to a group of servers instances to a new developer.
